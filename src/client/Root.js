import React from 'react';
import App from '../shared/App';

function Root() {
    return (
        <div>
            <App/>
        </div>
    );
}

export default Root;