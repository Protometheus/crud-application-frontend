import React from 'react';
import Axios from 'axios';

function DeleteView() {
    const onBtnDeleteClick = () => {
        let targetUserId = document.getElementById("inpTargetUser").value;

        Axios.delete("/users/" + targetUserId)
            .then((res) => {
                alert(res.data.title + " 결과 : " + res.data.desc);
                document.getElementById("inpTargetUser").value = "";
            })
            .catch((err) => {
                alert(err);
            });
    };
    
    const onBtnClearClick = () => {
        document.getElementById("inpTargetUser").value = "";
    }

    return (
        <div className="view">
            <h3>Delete User</h3>
            <div>
                <input id="inpTargetUser" className="input input-form"/>
                <button id="btnDelete" className="btn btn-primary" onClick={onBtnDeleteClick}>Delete</button>
                <button id="btnClear" className="btn btn-warning" onClick={onBtnClearClick}>Clear</button>
            </div>
        </div>
    );
}

export default DeleteView;