import React from 'react';
import Axios from 'axios';

function CreateView() {
    // 직접 DOM에 접근하지 말고 ref 속성을 이용하면 더 좋은 코드로 작성 가능
    const onBtnSubmitClick = () => {
        let data =  {
            userId : document.getElementById('inpUserId').value,
            userPw : document.getElementById('inpUserPw').value,
            email : document.getElementById('inpEmail').value,
            tel : document.getElementById('inpTel').value,
            address : document.getElementById('inpAddress').value,
        }

        // URL - /crud-backend -> pakage.json - proxy 참조
        Axios.post("/users", data)
            .then((resInfo) => {
                alert(resInfo.data.title + " : " + resInfo.data.desc);
            })
            .catch((error) => {
                alert(error);
            });

        onBtnResetClick();
    };

    const onBtnResetClick = () => {
        document.getElementById('inpUserId').value = "";
        document.getElementById('inpUserPw').value = "";
        document.getElementById('inpEmail').value = "";
        document.getElementById('inpTel').value = "";
        document.getElementById('inpAddress').value = "";
    };

    return (
        <div className="view">
            <h3>Create User</h3>
            <br/>
            <input type="text" id="inpUserId" className="input input-form" placeholder="User ID"/>
            <br/>
            <input type="password" id="inpUserPw" className="input input-form" placeholder="User PW"/>
            <br/>
            <input type="email" id="inpEmail" className="input input-form" placeholder="Email"/>
            <br/>
            <input type="tel" id="inpTel" className="input input-form" placeholder="Tel"/>
            <br/>
            <input type="text" id="inpAddress" className="input input-form" placeholder="Address"/>
            <br/>
            <div>
                <button className="btn btn-primary" type="button" onClick={onBtnSubmitClick}>Submit</button>
                &nbsp;
                <button className="btn btn-warning" type="reset" onClick={onBtnResetClick}>Reset</button>
            </div>
        </div>
    );
}

export default CreateView;