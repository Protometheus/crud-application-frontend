import React from 'react';
import Axios from 'axios';

function UpdateView() {
    const onSearchClick = () => {
        let targetId = document.getElementById("inpTargetUserId").value;

        if (targetId === "" || targetId === undefined || targetId === null) {
            alert("아이디를 입력하십시오.");

            return;
        }

        Axios.get("/users/" + targetId)
            .then((res) => {
                if (res.data === "" || res.data === undefined || res.data === null) {
                    alert("해당하는 ID가 존재하지 않습니다.");
                } else {
                    console.log(res);
                    document.getElementById("lblTargetUserId").innerHTML = "Target User - " + res.data.userId;
                    document.getElementById("inpUserId").value = res.data.userId;
                    document.getElementById("inpUserPw").value = res.data.userPw;
                    document.getElementById("inpEmail").value = res.data.email;
                    document.getElementById("inpTel").value = res.data.tel;
                    document.getElementById("inpAddress").value = res.data.address; 
                }
            })
            .catch((err) => {
                alert(err);
            });
    };

    const onBtnClearTargetUserClick = () => {
        document.getElementById("inpTargetUserId").value = "";
        document.getElementById("lblTargetUserId").innerHTML = "Target User - None";
    };

    const onBtnUpdateClick = () => {
        let updateData = {
            userId : document.getElementById("inpUserId").value,
            userPw : document.getElementById("inpUserPw").value,
            email : document.getElementById("inpEmail").value,
            tel : document.getElementById("inpTel").value,
            address : document.getElementById("inpAddress").value,
        }

        Axios.put("/users", updateData)
            .then((res) => {
                alert(res.data.title + " 결과 : " + res.data.desc);
            })
            .catch((err) => {
                alert(err);
            });
    };

    const onBtnClearUpdateFormClick = () => {
        document.getElementById("inpUserId").value = "";
        document.getElementById("inpUserPw").value = "";
        document.getElementById("inpEmail").value = "";
        document.getElementById("inpTel").value = "";
        document.getElementById("inpAddress").value = "";
    };

    return (
        <div className="view">
            <h3>Update User</h3>
            <div>
                <input id="inpTargetUserId" type="text" className="input input-form"/>
                <button id="btnSearch" className="btn btn-primary" onClick={onSearchClick}>Search</button>
                <button id="btnClearTargetUser" className="btn btn-warning" onClick={onBtnClearTargetUserClick}>Clear</button>
            </div>
            <br/>
            <h4>
                <label id="lblTargetUserId">Target User - None</label>
            </h4>
            <br/>
            <label>ID </label>
            <input id="inpUserId" type="text" className="input input-form" readOnly/>
            <br/>
            <label>PW </label>
            <input id="inpUserPw" type="password" className="input input-form"/>
            <br/>
            <label>Email </label>
            <input id="inpEmail" type="text" className="input input-form" readOnly/>
            <br/>
            <label>Tel </label>
            <input id="inpTel" type="text" className="input input-form"/>
            <br/>
            <label>Address </label>
            <input id="inpAddress" type="text" className="input input-form"/>
            <br/>
            <div>
                <button id="btnUpdate" className="btn btn-primary" onClick={onBtnUpdateClick}>Update</button>
                <button id="btnClear" className="btn btn-warning" onClick={onBtnClearUpdateFormClick}>Clear</button>
            </div>
            <h4>
                <label id="lblResultMsg"></label>
            </h4>
        </div>
    );
}

export default UpdateView;