import React from 'react';
import Axios from 'axios';

const List = ({ userListData }) => (
  <ul>
      {
          userListData.map((element, index) => <li key={index}>{element.userId}</li> )
      }
  </ul>  
);

class ReadView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userListData : []
        }
    }

    onRetrieveUserClick = () => {
        if (document.getElementById("inpUserId").value === "") {
            alert("Please enter an ID to search for");

            return;
        }

        let targetUserId = document.getElementById("inpUserId").value;

        Axios.get("/users/" + targetUserId)
            .then((res) => {
                if (res.data === "" || res.data === undefined || res.data === null) {
                    document.getElementById("lblFoundUser").innerHTML = "Not Found";
                } else {
                    document.getElementById("lblFoundUser").innerHTML = res.data.userId;
                }
            })
            .catch((err) => {
                alert(err);
            });
    };

    onClearFoundUserClick = () => {
        document.getElementById("inpUserId").value = "";
        document.getElementById("lblFoundUser").innerHTML = "None";
    };

    onClearUserListClick = () => {
        this.setState({ userListData : [] });
    }

    onRetrieveAllUserClick = () => {
        Axios.get("/users")
            .then((res) => {
                this.setState({ userListData : res.data });
            })
            .catch((err) => {
                console.log(err);
            });
    };

    render () {
        return (
                <div className="view">
                    <h3>Search User</h3>
                    <div>
                    <input id="inpUserId" className="input input-form" placeholder="Find By User ID" />
                    &nbsp;
                    <button id="btnRetrieveUser" className="btn btn-primary" onClick={this.onRetrieveUserClick} type="submit">Search</button>
                    <button className="btn btn-warning" onClick={this.onClearFoundClick} type="reset">Clear</button>
                </div>
                <h3>Found User</h3>
                <label id="lblFoundUser">None</label>
                <h3>User List</h3>
                <div>
                    <button id="btnRetriveAll" className="btn btn-primary" onClick={this.onRetrieveAllUserClick}>Retrieve All</button>
                    <button className="btn btn-warning" onClick={this.onClearUserListClick} >Clear</button>
                </div>
                <List userListData={this.state.userListData}/>
            </div>
        );
    };
}

export default ReadView;