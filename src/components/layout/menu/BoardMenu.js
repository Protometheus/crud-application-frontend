import React from 'react';
import { NavLink, Redirect, Route, Switch } from 'react-router-dom';

import BoardView from '../../board/BoardView';

function BoardMenu() {
    return (
        <div>
            <div className="Menu">
                <NavLink to="/board/create">
                    <button className="btn btn-primary">Create</button>
                </NavLink>
                &nbsp;
                <NavLink to="/board/read">
                    <button className="btn btn-info">Read</button>
                </NavLink>
                &nbsp;
                <NavLink to="/board/update">
                    <button className="btn btn-warning">Update</button>
                </NavLink>
                &nbsp;
                <NavLink to="/board/delete">
                    <button className="btn btn-danger">Delete</button>
                </NavLink>
            </div>
            <hr/>
            <Switch>
                <Route exact path="/board">
                    <Redirect to="/board/read"/>
                </Route>
                <Route path="/board/create">
                    <BoardView/>
                </Route>
                <Route path="/board/read">
                    <BoardView/>
                </Route>
                <Route path="/board/update">
                    <BoardView/>
                </Route>
                <Route path="/board/delete">
                    <BoardView/>
                </Route>
            </Switch>
        </div>
    );
}

export default BoardMenu;