import React from 'react';
import { NavLink, Switch, Route } from 'react-router-dom';

import UserMenu from './UserMenu';
import BoardMenu from './BoardMenu';

function HomeMenu() {
    return (
        <div>
            <div className="Menu">
                <NavLink to="/home">
                    <button className="btn btn-main">Home</button>
                </NavLink>
                &nbsp;
                <NavLink to="/board">
                    <button className="btn btn-primary">Board</button>
                </NavLink>
                &nbsp;
                <NavLink to="/user">
                    <button className="btn btn-info">User</button>
                </NavLink>
            </div>
            <hr/>
            <div>
                <Switch>
                    <Route path="/board">
                        <BoardMenu/>
                    </Route>
                    <Route path="/user">
                        <UserMenu/>
                    </Route>
                </Switch>
            </div>
        </div>
    );
}

export default HomeMenu;