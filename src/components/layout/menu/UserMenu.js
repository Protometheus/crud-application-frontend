import React from 'react';
import { NavLink, Switch, Redirect, Route } from 'react-router-dom';

import CreateView from '../../user/CreateView';
import ReadView from '../../user/ReadView';
import UpdateView from '../../user/UpdateView';
import DeleteView from '../../user/DeleteView';

import './UserMenu.css';

function UserMenu() {
    return (
        <div>
            <div className="Menu">
                <NavLink to="/user/create">
                    <button className="btn btn-primary">Create</button>
                </NavLink>
                &nbsp;
                <NavLink to="/user/read">
                    <button className="btn btn-info">Read</button>
                </NavLink>
                 &nbsp;
                <NavLink to="/user/update">
                    <button className="btn btn-warning">Update</button>
                </NavLink>
                &nbsp;
                <NavLink to="/user/delete">
                    <button className="btn btn-danger">Delete</button>
                </NavLink>
            </div>
            <hr/>
            <div>
                <Switch>
                    <Route exact path="/user">
                        <Redirect to="/user/read"/>
                    </Route>
                    <Route path="/user/create">
                        <CreateView/>
                    </Route>
                    <Route path="/user/read">
                        <ReadView/>
                    </Route>
                    <Route path="/user/update">
                        <UpdateView/>
                    </Route>
                    <Route path="/user/delete">
                        <DeleteView/>
                    </Route>
                </Switch>
            </div>
        </div>
    );
}

export default UserMenu;