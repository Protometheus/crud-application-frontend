import React from 'react';

import './Header.css';

function Header() {
    return (
        <header>
            <div className="header">
                <h1>CRUD Application</h1>
            </div>
        </header>
    );
}

export default Header;