import React from 'react';

import './View.css';

function HomeView() {
    return(
        <div className="view">
            <h1>Home</h1>
            <h3>Welcome</h3>
        </div>
    );
}

export default HomeView;