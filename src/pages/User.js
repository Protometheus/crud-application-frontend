import React from 'react';

import Header from '../components/layout/Header';
import MainMenu from '../components/layout/menu/MainMenu';

function User() {
    return (
        <div>
            <Header/>
            <MainMenu/>
        </div>
    );
}

export default User;