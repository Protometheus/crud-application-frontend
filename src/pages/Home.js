import React from 'react';

import Header from '../components/layout/Header';
import MainMenu from '../components/layout/menu/MainMenu';
import HomeView from '../components/home/HomeView';

function Home() {
    return (
        <div>
            <Header/>
            <MainMenu/>
            <HomeView/>
        </div>
    );
}

export default Home;