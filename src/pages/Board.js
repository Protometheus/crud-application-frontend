import React from 'react';

import Header from '../components/layout/Header';
import MainMenu from '../components/layout/menu/MainMenu';

function Board() {
    return (
        <div>
            <Header/>
            <MainMenu/>
        </div>
    );
}

export default Board;