import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import Home from '../pages/Home';
import User from '../pages/User';
import Board from '../pages/Board';

import './App.css';
import '../common/common.css';

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route exact path="/">
            <Redirect to="/home"/>
          </Route>
          <Route path="/home">
            <Home/>
          </Route>
          <Route path="/board">
            <Board/>
          </Route>
          <Route path="/user">
            <User/>
          </Route>
          <Route path="/*">
            <Redirect to="/home"/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
